package com.epam.rd.autotasks;

/**
 * The Task class represents a task that needs to be completed by an employee.
 * Each task has a description, an assignee, and a reviewer.
 * The class provides methods to get and set these properties.
 */
public class Task {
    /**
     * The description of the task
     */
    private String description;

    /**
     * The employee to whom the task is assigned
     */
    private Employee assignee;

    /**
     * The employee who reviews the task
     */
    private Employee reviewer;

    /**
     * Constructs a Task object with the given description, assignee, and reviewer.
     *
     * @param description the description of the task
     * @param assignee    the employee to whom the task is assigned
     * @param reviewer    the employee who reviews the task
     */
    public Task(String description, Employee assignee, Employee reviewer) {
        this.description = description;
        this.assignee = assignee;
        this.reviewer = reviewer;
    }

    /**
     * Returns the description of the task.
     *
     * @return the description of the task
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the task.
     *
     * @param description the description of the task
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the employee to whom the task is assigned.
     *
     * @return the employee to whom the task is assigned
     */
    public Employee getAssignee() {
        return assignee;
    }

    /**
     * Sets the employee to whom the task is assigned.
     *
     * @param assignee the employee to whom the task is assigned
     */
    public void setAssignee(Employee assignee) {
        this.assignee = assignee;
    }

    /**
     * Returns the employee who reviews the task.
     *
     * @return the employee who reviews the task
     */
    public Employee getReviewer() {
        return reviewer;
    }

    /**
     * Sets the employee who reviews the task.
     *
     * @param reviewer the employee who reviews the task
     */
    public void setReviewer(Employee reviewer) {
        this.reviewer = reviewer;
    }
}
