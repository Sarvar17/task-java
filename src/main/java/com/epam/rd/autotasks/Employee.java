package com.epam.rd.autotasks;

/**
 * The Employee class represents an individual employee in a company.
 * Each employee has a name and a position within the company.
 * The class provides methods to get and set these properties.
 */
public class Employee {
    /**
     * The name of the employee
     */
    private String name;
    /**
     * The position of the employee
     */
    private String position;

    /**
     * Constructs an Employee object with the given name and position.
     *
     * @param name     the name of the employee
     * @param position the position of the employee
     */
    public Employee(String name, String position) {
        this.name = name;
        this.position = position;
    }

    /**
     * Returns the name of the employee.
     *
     * @return the name of the employee
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the employee.
     *
     * @param name the name of the employee
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the position of the employee.
     *
     * @return the position of the employee
     */
    public String getPosition() {
        return position;
    }

    /**
     * Sets the position of the employee.
     *
     * @param position the position of the employee
     */
    public void setPosition(String position) {
        this.position = position;
    }
}
