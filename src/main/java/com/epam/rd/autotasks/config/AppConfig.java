package com.epam.rd.autotasks.config;

import com.epam.rd.autotasks.Employee;
import com.epam.rd.autotasks.Task;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * AppConfig is a configuration class for the Spring Framework.
 * It declares beans for Employee and Task objects, which are used throughout the application.
 * <br/><br/>
 * This class utilizes the Spring @Configuration annotation to indicate that it is a source of bean definitions,
 * and the @Bean annotation to signal that a method will return an object that should be registered as a bean in the Spring application context.
 */
@Configuration
public class AppConfig {
    /**
     * The name of the assignee.
     */
    private static final String ASSIGNEE_NAME = "John Doe";
    /**
     * The position of the assignee.
     */
    private static final String ASSIGNEE_POSITION = "Junior Software Engineer";
    /**
     * The name of the reviewer.
     */
    private static final String REVIEWER_NAME = "Emily Brown";
    /**
     * The position of the reviewer.
     */
    private static final String REVIEWER_POSITION = "Senior Software Engineer";
    /**
     * The description of the task.
     */
    private static final String TASK_DESCRIPTION = "New feature";

    /**
     * Returns an Employee object representing the assignee.
     *
     * @return an Employee object representing the assignee
     */
    @Bean("assignee")
    public Employee assignee() {
        return new Employee(ASSIGNEE_NAME, ASSIGNEE_POSITION);
    }

    /**
     * Returns an Employee object representing the reviewer.
     *
     * @return an Employee object representing the reviewer
     */
    @Bean("reviewer")
    public Employee reviewer() {
        return new Employee(REVIEWER_NAME, REVIEWER_POSITION);
    }

    /**
     * Returns a Task object with the given assignee and reviewer.
     *
     * @param assignee the employee to whom the task is assigned
     * @param reviewer the employee who reviews the task
     * @return a Task object with the given assignee and reviewer
     */
    @Bean
    public Task task(@Qualifier("assignee") Employee assignee,
                     @Qualifier("reviewer") Employee reviewer) {
        return new Task(TASK_DESCRIPTION, assignee, reviewer);
    }
}